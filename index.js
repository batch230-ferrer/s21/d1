//console.log("Konnichiwa");

// S20 REVIEW
		// 0         true			1
		// 1         true			2
		// 2		 true			3
		// 3		 true			4
		// 4         true           5
		// 5         true  
/*for(let number = 0; number < 10; number++){
	if(number == 2){ // false // false // true = display message // false // false
		console.log("Number 2 is found, skip the next line of codes then continue the loop");
		continue;
	}

	if(number != 2){ // true - display // display //display // display // display
		console.log(number);
	}

	if(number == 5){ // false //false // false // false // true
		console.log("Stop the loop at number 5");
		break;
	}
}*/

/*
	1. Initialize a value
	2. Checks the condition
	3. Runs the statements inside the loop if the condition is true
	4. Change of value (increment/decrement)
		- go back to step 2

*/

// Reassigning with concatenation of String
/*
let myString = "dooooom";
let letterO = "";

for(let i=0; i<myString.length; i++){
	if(myString[i] == 'o'){
		//        emptyString + o
		//			o 	  +     o
		//		    oo    +     o
		//			ooo   +     o
		//			oooo  + 	o
		//			ooooo
		letterO = letterO + myString[i];

	}
}
console.log(letterO);

*/


// -------------------------------------------------------


// ARRAY

// An array in programming is simply a list of data. Let's write the example earlier.

// a
let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

// b
let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// Common examples of arrays

// Same data type elements of an array
// Index		0     1     2     3
let grades = [98.5, 94.3 , 89.2, 90.1, 99];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative ways to write arrays

let myTasks = 
[
	'drink html',
	'eat javascript',
	'inhale css',
	'bake bootstrap'
];

console.log(myTasks);

// Creating an array with values from variable

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = 
[
	city1, city2, city3

];

console.log(cities)

// [SECTION] length property
// The .length property allows us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

console.log("______");


// length property can also be used with strings. Some array methods and properties can also be used with strings

let fullname = "Jamie Oliver";
console.log(fullname.length);

// Removing the last element in an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

cities.length--;
console.log(cities.length);
console.log(cities);


// However, We can't do the same on strings
console.log("Initial length of full name: "+ fullname.length);
fullname.length = fullname.length-1;
console.log("Current length of fullname: " + fullname.length);
console.log(fullname);

/*let theBeatles =
[
	'John',
	'Paul',
	'Ringo',
	'George'
];

theBeatles++;

console.log(theBeatles);*/


// Accessing the element of an array through index
// let grades = [98.5, 94.3 , 89.2, 90.1, 99];

console.log(grades[0]);

//let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
console.log(computerBrands[3]);

//let grades = [98.5, 94.3 , 89.2, 90.1, 99];

function getGrade(index){
	console.log(grades[index]);
}

getGrade(3);
getGrade(grades.length-1);
console.log("______");
let lakersLegend = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
let currentLaker = lakersLegend[2];
console.log(currentLaker);

// Change an element / reassigning array values

console.log('Array before reassignment');
console.log(lakersLegend);
lakersLegend[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegend);

// Change the last element

let bullsLegend = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
let lastElementIndex = bullsLegend.length-1;


console.log(bullsLegend[lastElementIndex]);

// You could also access it directly
console.log(bullsLegend[bullsLegend.length-1]);
bullsLegend[lastElementIndex] = "Harper";

console.log(bullsLegend);

// Add items into the array


let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[2] = "TIfa Lockhart";
console.log(newArr);

					//      0 		1          2                3
					//		1 		 2  		3
// Current output: ['Cloud Strife', empty, 'Tifa Lockhard']
// Adding elements after the last element

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);


// Looping over an array
for(let index=0; index<newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];

// a loop that will if check per an element if divisible by 5
for(let index=0; index<numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}
	else{
		console.log(numArr[index] + " is not divisible 5");
	}
}


// [SECTION] Multidimensional Arrays

// arrays inside an array
let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

console.log(chessBoard[1][4])
					//row // col

// Mini Activity -Display c5
console.log(chessBoard[4][2]);

console.log("Pawn moves to:  "+ chessBoard[1][5]);